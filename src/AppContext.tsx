import React, { createContext, useState } from "react";
import { User } from "./User";

type UserStateType = {
  user: User;
  changeUser: React.Dispatch<React.SetStateAction<User>>;
};

const AppContext = () => {
  const init = {
    id: 0,
    fullName: "",
    balance: 0,
  };

  const initContext: UserStateType = {
    user: init,
    changeUser: (e) => {},
  };

  const UserContext = createContext<UserStateType>(initContext);

  const UserProvider = (props: any) => {
    const [user, setUser] = useState<User>(init);
    const userState: UserStateType = { user: user, changeUser: setUser };
    return (
      <UserContext.Provider value={userState}>
        {props.children}
      </UserContext.Provider>
    );
  };
  return {
    UserContext,
    UserProvider,
  };
};

export default AppContext();
