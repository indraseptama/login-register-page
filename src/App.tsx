import React, { useContext, useState, useEffect } from "react";
import { Redirect, Route } from "react-router-dom";
import { IonApp, IonRouterOutlet } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import Login from "./pages/Login";
import Main from "./pages/Main";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
import { AppContextProvider, AppContext } from "./store/Core";

const App: React.FC = () => {
  return (
    <AppContextProvider>
      <IonApp>
        <StartApp />
      </IonApp>
    </AppContextProvider>
  );
};

const StartApp: React.FC = () => {
  const { state, dispatch } = useContext(AppContext);
  const { emailAddress } = state.user;
  const [emailState, setEmail] = useState(emailAddress);

  useEffect(() => {
    setEmail(state.user.email);
  }, [state]);

  console.log(emailAddress);
  return (
    <IonReactRouter>
      <IonRouterOutlet>
        <Route path="/login" component={Login} exact={true} />
        <Route path="/page" component={Main} />
        <Route
          exact={true}
          path="/"
          render={() => <Redirect to={emailAddress ? "/page" : "/login"} />}
        />
        <Route
          exact={true}
          path="/page"
          render={() => <Redirect to="/page/home" />}
        />
      </IonRouterOutlet>
    </IonReactRouter>
  );
};

export default App;
