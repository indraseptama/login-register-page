import { userReducer } from "./reducers/UserReducer";

interface State {
  user: any;
}

export function reducer({ user }: State, action: any): State {
  return {
    user: userReducer(user, action),
  };
}
