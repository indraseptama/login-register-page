export const user = {
  name: "",
  balance: 0,
  emailAddress: "",
};

export const userActionType = {
  setName: "USER_SET_NAME",
  setBalance: "USER_SET_BALANCE",
  setEmailAddress: "USER_SET_EMAIL_ADDRESS",
  logOut: "USER_LOGOUT",
};

export function userReducer(state: any, action: any) {
  switch (action.type) {
    case userActionType.setName:
      return { ...state, name: action.value };
    case userActionType.setBalance:
      return { ...state, balance: action.value };
    case userActionType.setEmailAddress:
      return { ...state, emailAddress: action.value };
    case userActionType.logOut:
      return { user };
    default:
      return state;
  }
}
