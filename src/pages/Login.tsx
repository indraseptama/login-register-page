import {
  IonContent,
  IonPage,
  IonButton,
  IonGrid,
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonInput,
  IonFooter,
  IonLoading,
  IonImg,
  IonAlert,
} from "@ionic/react";
import React, { useState, useContext } from "react";
import axios from "axios";
import "./Login.css";
import { RouteComponentProps } from "react-router";
import { AppContext } from "../store/Core";
import { userActionType } from "../store/reducers/UserReducer";

const Login: React.FC<RouteComponentProps> = (props) => {
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const [isError, setError] = useState(false);

  const { dispatch } = useContext(AppContext);

  const loginUser = () => {
    setLoading(true);
    axios
      .post(`https://reqres.in/api/login`, {
        email: username,
        password: password,
      })
      .then((res) => {
        setLoading(false);
        if (res.status === 200) {
          dispatch({ type: userActionType.setEmailAddress, value: username });
          dispatch({ type: userActionType.setBalance, value: 1 });
          dispatch({ type: userActionType.setName, value: username });
          props.history.replace("page");
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(true);
      });
  };

  return (
    <React.Fragment>
      <IonLoading isOpen={isLoading} message="Please wait"></IonLoading>
      <IonAlert
        isOpen={isError}
        onDidDismiss={() => setError(false)}
        message="Username dan password salah"
        buttons={[
          {
            text: "ok",
            handler: (e) => {
              setError(false);
            },
          },
        ]}
      />
      <IonPage>
        <IonContent className="ion-padding">
          <IonGrid className="fullheight xc">
            <IonRow>
              <IonCol>
                <IonImg
                  className="img-logo ion-align-items-center"
                  src="https://lh3.googleusercontent.com/cGgUp5Cj-yk6Toy9Fd79ln-gtv5qFhixYA6aQndub8P3uxWTuhuhHYrXCQxxanTXSor_"
                />
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol>
                <IonItem>
                  <IonLabel position="floating">username</IonLabel>
                  <IonInput
                    onIonChange={(e: any) => setUsername(e.target.value)}
                  ></IonInput>
                </IonItem>
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol>
                <IonItem>
                  <IonLabel position="floating">password</IonLabel>
                  <IonInput
                    type="password"
                    onIonChange={(e: any) => setPassword(e.target.value)}
                  ></IonInput>
                </IonItem>
              </IonCol>
            </IonRow>
            <IonButton
              onClick={loginUser}
              disabled={!username || !password}
              expand="full"
            >
              Login
            </IonButton>
          </IonGrid>
        </IonContent>
        <IonFooter>
          <IonGrid>
            <IonRow>
              <IonCol>
                <IonItem className="ion-text-center">
                  <IonLabel>Sign Up</IonLabel>
                </IonItem>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonFooter>
      </IonPage>
    </React.Fragment>
  );
};

export default Login;
