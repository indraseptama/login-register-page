import React from "react";
import {
  IonTabs,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonIcon,
  IonLabel,
} from "@ionic/react";
import { wallet, person, planet } from "ionicons/icons";
import { IonReactRouter } from "@ionic/react-router";
import { Route, Redirect, RouteComponentProps } from "react-router";
import Home from "./Home";
import Insight from "./Insight";
import Tool from "./Tool";
import TopUp from "./TopUp";
const Main: React.FC<RouteComponentProps> = ({ match }) => {
  return (
    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          <Route
            path={`${match.url}/:tab(home)`}
            component={Home}
            exact={true}
          />
          <Route
            path={`${match.url}/:tab(tool)`}
            component={Tool}
            exact={true}
          />
          <Route
            path={`${match.url}/:tab(insight)`}
            component={Insight}
            exact={true}
          />
          <Route path={`${match.url}/:tab(home-topup)`} component={TopUp} />
          <Route
            exact={true}
            path={`${match.url}`}
            render={() => <Redirect to={`${match.url}/home`} />}
          />
          <Route
            exact={true}
            path="/"
            render={() => <Redirect to={`${match.url}/home`} />}
          />
        </IonRouterOutlet>
        <IonTabBar slot="bottom">
          <IonTabButton href={`${match.url}/home`} tab="home">
            <IonIcon icon={planet} />
            <IonLabel>Home</IonLabel>
          </IonTabButton>
          <IonTabButton href={`${match.url}/tool`} tab="tool">
            <IonIcon icon={wallet} />
            <IonLabel>My Wallet</IonLabel>
          </IonTabButton>
          <IonTabButton href={`${match.url}/insight`} tab="insight">
            <IonIcon icon={person} />
            <IonLabel>Account</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonReactRouter>
  );
};

export default Main;
