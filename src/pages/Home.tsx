import {
  IonContent,
  IonPage,
  IonCard,
  IonCardContent,
  IonGrid,
  IonRow,
  IonCol,
  IonIcon,
  IonLabel,
  IonButton,
  IonText,
} from "@ionic/react";
import React, { useContext } from "react";
import "./Home.scss";
import {
  scan,
  addCircleOutline,
  arrowUpCircleOutline,
  arrowDownCircleOutline,
  walletOutline,
  notificationsOutline,
  logoBitcoin,
} from "ionicons/icons";
import { AppContext } from "../store/Core";
import { withRouter } from "react-router";

const Home: React.SFC<any> = (props) => {
  const { state, dispatch } = useContext(AppContext);
  const { emailAddress, balance } = state.user;
  console.log(emailAddress);
  return (
    <IonPage>
      <IonContent scrollEvents={true}>
        <div className="top white-color">
          <IonGrid
            fixed={true}
            className="ion-padding-top ion-padding-horizontal"
          >
            <IonRow>
              <IonCol size="8">
                <IonIcon className="top-icon" icon={walletOutline} />
                <IonLabel className="ion-margin-start">Rp. {balance} </IonLabel>
              </IonCol>

              <IonCol className="ion-text-end top-icon" size="4">
                <IonIcon icon={notificationsOutline} />
              </IonCol>
            </IonRow>
            <IonRow className="ion-text-center four-icon">
              <IonCol>
                <IonIcon
                  icon={scan}
                  size="large"
                  onClick={() => console.log("hehe")}
                ></IonIcon>
                <div>
                  <small>Scan</small>
                </div>
              </IonCol>
              <IonCol>
                <IonIcon
                  icon={addCircleOutline}
                  size="large"
                  onClick={(e) => {
                    e.preventDefault();
                    props.history.push(`/page/home-topup`);
                  }}
                ></IonIcon>
                <div>
                  <small>Top up</small>
                </div>
              </IonCol>
              <IonCol>
                <IonIcon icon={arrowUpCircleOutline} size="large"></IonIcon>
                <div>
                  <small>Send</small>
                </div>
              </IonCol>
              <IonCol>
                <IonIcon icon={arrowDownCircleOutline} size="large"></IonIcon>
                <div>
                  <small>Request</small>
                </div>
              </IonCol>
            </IonRow>
          </IonGrid>
        </div>
        <div className="content">
          <div className="background"></div>
          <IonCard>
            <IonCardContent>
              <IonGrid>
                <IonRow>
                  <IonCol className="ion-text-start" size="2">
                    <IonIcon className="logo-bitcoin" icon={logoBitcoin} />
                  </IonCol>
                  <IonCol size="6.5">
                    <IonText>
                      <h3 className="trf-text">Transfer to Bank</h3>
                      <p>Transfer cepat & gampang!</p>
                    </IonText>
                  </IonCol>
                  <IonCol size="3.5">
                    <IonButton expand="full">SEND</IonButton>
                  </IonCol>
                </IonRow>
                <IonRow className="ion-text-center">
                  <IonCol>
                    <IonIcon
                      icon={scan}
                      size="large"
                      onClick={() => console.log("hehe")}
                    ></IonIcon>
                    <div>
                      <small>Scan</small>
                    </div>
                  </IonCol>
                  <IonCol>
                    <IonIcon icon={addCircleOutline} size="large"></IonIcon>
                    <div>
                      <small>Top up</small>
                    </div>
                  </IonCol>
                  <IonCol>
                    <IonIcon icon={arrowUpCircleOutline} size="large"></IonIcon>
                    <div>
                      <small>Send</small>
                    </div>
                  </IonCol>
                  <IonCol>
                    <IonIcon
                      icon={arrowDownCircleOutline}
                      size="large"
                    ></IonIcon>
                    <div>
                      <small>Request</small>
                    </div>
                  </IonCol>
                </IonRow>
                <IonRow className="ion-text-center">
                  <IonCol>
                    <IonIcon
                      icon={scan}
                      size="large"
                      onClick={() => console.log("hehe")}
                    ></IonIcon>
                    <div>
                      <small>Scan</small>
                    </div>
                  </IonCol>
                  <IonCol>
                    <IonIcon icon={addCircleOutline} size="large"></IonIcon>
                    <div>
                      <small>Top up</small>
                    </div>
                  </IonCol>
                  <IonCol>
                    <IonIcon icon={arrowUpCircleOutline} size="large"></IonIcon>
                    <div>
                      <small>Send</small>
                    </div>
                  </IonCol>
                  <IonCol>
                    <IonIcon
                      icon={arrowDownCircleOutline}
                      size="large"
                    ></IonIcon>
                    <div>
                      <small>Request</small>
                    </div>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonCardContent>
          </IonCard>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default withRouter(Home);
