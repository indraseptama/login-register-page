import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
} from "@ionic/react";
import React, { useContext } from "react";
import { AppContext } from "../store/Core";
import { userActionType } from "../store/reducers/UserReducer";
import { RouteComponentProps } from "react-router";

const Insight: React.FC<RouteComponentProps> = (props) => {
  const { state, dispatch } = useContext(AppContext);
  const { emailAddress } = state.user;
  return (
    <IonPage>
      <IonContent fullscreen>
        <IonGrid>
          <IonRow>
            <IonCol>{emailAddress}</IonCol>
            <IonCol>
              <IonButton
                color="danger"
                onClick={(e) => {
                  e.preventDefault();
                  dispatch({ type: userActionType.logOut, value: "" });
                  props.history.replace(`/login`);
                }}
              >
                Logout
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Insight;
