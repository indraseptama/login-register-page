export type User = {
  id: number;
  fullName: string;
  balance: number;
};
